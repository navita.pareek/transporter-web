// const HOST = 'http://transporter-network.us-east-2.elasticbeanstalk.com';
const HOST = 'http://localhost:8080';
export default {
  SITE_PREFIX: `${HOST}`,
  LOGIN_USER: () => `${HOST}/user/login`,
  REGISTER_USER: () => `${HOST}/user/register`,
  FETCH_STATES_CITIES: (phoneNumber, language) => `${HOST}/support/states/cities/${phoneNumber}/language/${language}`,
  SEARCH_VEHICLE: () => `${HOST}/trip/add`,
  SEARCH_LOAD: () => `${HOST}/trip/search`,
  UPDATE_TRIP_STATUS: () => `${HOST}/trip/update`,
  UPDATE_VEHICLE_STATUS: () => `${HOST}/vehicle/update`,
  ADD_VEHICLE: () => `${HOST}/vehicle/add`,
  ADD_COMPANY: () => `${HOST}/company/add`,
  ADD_MANPOWER: () => `${HOST}/user/manpower/add`,
  REMOVE_VEHICLE: () => `${HOST}/vehicle/remove`,
  REMOVE_MANPOWER: () => `${HOST}/user/manpower/remove`,
  UPDATE_FEEDBACK: () => `${HOST}/support/feedback`,
  GET_MY_VEHICLES: (phoneNumber) => `${HOST}/user/vehicle/${phoneNumber}`,
  GET_MY_TRIPS: (phoneNumber) => `${HOST}/trip/${phoneNumber}`,
};

import $ from "jquery";
import APIUrls from './apiUrls';

export default {

  loginUser: ( data, successCallback) => {
    $.ajax({
      url: APIUrls.LOGIN_USER(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  registerUser: ( data, successCallback) => {
    $.ajax({
      url: APIUrls.REGISTER_USER(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  getStateCities: ( phoneNumber, language, successCallback) => {
    $.ajax({
      url: APIUrls.FETCH_STATES_CITIES(phoneNumber, language),
      type: 'GET',
      contentType: 'application/json',
      success: successCallback
    });
  },

  searchVehicle:( data, successCallback) => {
    $.ajax({
      url: APIUrls.SEARCH_VEHICLE(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  searchLoad:( data, successCallback) => {
    $.ajax({
      url: APIUrls.SEARCH_LOAD(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  updateTripStatus:( data, successCallback) => {
    $.ajax({
      url: APIUrls.UPDATE_TRIP_STATUS(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  updateVehicleStatus:( data, successCallback) => {
    $.ajax({
      url: APIUrls.UPDATE_VEHICLE_STATUS(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  addVehicle:( data, successCallback) => {
    $.ajax({
      url: APIUrls.ADD_VEHICLE(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  removeVehicle:( data, successCallback) => {
    $.ajax({
      url: APIUrls.REMOVE_VEHICLE(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  removeManpower:( data, successCallback) => {
    $.ajax({
      url: APIUrls.REMOVE_MANPOWER(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  addCompany:( data, successCallback) => {
    $.ajax({
      url: APIUrls.ADD_COMPANY(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  addManpower:( data, successCallback) => {
    $.ajax({
      url: APIUrls.ADD_MANPOWER(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  updateFeedback:( data, successCallback) => {
    $.ajax({
      url: APIUrls.UPDATE_FEEDBACK(),
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      success: successCallback
    });
  },

  getMyVehicles:( phoneNumber, successCallback) => {
    $.ajax({
      url: APIUrls.GET_MY_VEHICLES(phoneNumber),
      type: 'GET',
      contentType: 'application/json',
      success: successCallback
    });
  },

  getMyTrips:( phoneNumber, successCallback) => {
    $.ajax({
      url: APIUrls.GET_MY_TRIPS(phoneNumber),
      type: 'GET',
      contentType: 'application/json',
      success: successCallback
    });
  },
};

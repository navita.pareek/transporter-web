import React, {PropTypes} from 'react';
import {Button, Radio, Checkbox} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class LetsConnect extends React.Component {

  componentDidMount() {
    this.isHappy = "true";
  }

  onLetsConnectClick(){
    this.props.onLetsConnectClick(this.props.userPhoneNumber,
      this.isHappy,
      document.getElementById('wantCallBack').checked,
    );
  }

  updateIsHappy(isHappyInput){
    this.isHappy = isHappyInput;
  }

  render(){
    return (
      <div>
        <h2 style={style.titleStyle}>Connect with Transport Network Team</h2>
        <p style={style.textStyle} id="pageTitle">
          Are you happy using Transport Network?
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <Radio name="groupOptions" value="true" defaultChecked="true" onClick={() => this.updateIsHappy("true")} >Yes</Radio>
              <Radio name="groupOptions" value="false" onClick={() => this.updateIsHappy("false")}>No</Radio>
            </Row>
            <Row className="row-container" >
              <Checkbox id="wantCallBack" style={style.dashboardCheckboxStyle} defaultChecked>Want to talk to transport network team?</Checkbox>
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onLetsConnectClick.bind(this)}>Submit Connect Request</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default LetsConnect;

LetsConnect.propTypes = {
  userPhoneNumber: PropTypes.string.isRequired,
  onLetsConnectClick: PropTypes.func.isRequired,
};

import React, {PropTypes} from 'react';
import {Button} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class UpdateVehicleStatus extends React.Component {

  componentDidMount() {
    this.props.getMyVehicles(this.props.userPhoneNumber);
  }

  resetCitiesAsPerSelection(states,cities) {
    cities.selectedIndex = 0;

    while (cities.options.length > 1) {
      cities.remove(0);
    }

    if(states.selectedIndex == 0)
      return;

    for (let i = 0; i < this.props.stateCities[states.options[states.selectedIndex].text].length; i++) {
      this.createOption(cities, this.props.stateCities[states.options[states.selectedIndex].text][i], i+1);
    }
  }

  createOption(ddl, text, index) {
    var opt = document.createElement('option');
    opt.text = text;
    ddl.options.add(opt, index);
  }

  onUpdateVehicleStatusClick(){
    this.updateFormLabelText();
    if(this.areRequiredEntriesFilled() && this.areRequiredEntriesValid()){
      this.props.onUpdateVehicleStatusClick(
        this.props.userPhoneNumber,
        this.props.vehicles[document.getElementById('vehicle').selectedIndex - 1],
        document.getElementById('currentState').selectedIndex,
        document.getElementById('currentCity').selectedIndex,
        document.getElementById('vehicleStatus').selectedIndex,
        document.getElementById('vehicle').selectedIndex - 1,
      );
    }
  }

  updateFormLabelText(){
    let title = document.getElementById('pageTitle');
    title.innerHTML = "Change the below fields to proceed update.";
    title.style["color"] = "rgb(51, 51, 51)";
  }

  areRequiredEntriesFilled(){
    if ((document.getElementById('vehicle').selectedIndex == 0) ||
    (document.getElementById('vehicleStatus').selectedIndex == 0) ||
      (document.getElementById('currentState').selectedIndex == 0)||
      (document.getElementById('currentCity').selectedIndex == 0)){
      let title = document.getElementById('pageTitle');
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  areRequiredEntriesValid(){
    if (document.getElementById('vehicleStatus').selectedIndex ===
      (this.props.vehicles[document.getElementById('vehicle').selectedIndex - 1].status)
      &&
      (document.getElementById('currentState').selectedIndex + ':' + document.getElementById('currentCity').selectedIndex) ===
      (this.props.vehicles[document.getElementById('vehicle').selectedIndex - 1].startLocation)){

      let title = document.getElementById('pageTitle');
      title.innerHTML = "New state, city and status selected is same as current state, city and status.";
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  render(){
    return (
      <div>
        <h2 style={style.titleStyle}>Update Vehicle Status</h2>
        <p style={style.textStyle} id="pageTitle">
          Change the below fields to proceed update.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <select id="vehicle" style={style.dropdownStyle}>
                <option>Select vehicle to update</option>
                {
                  this.props.vehicles.map((vehicle) =>
                  {
                    return (<option key={this.props.vehicles.indexOf(vehicle)} value={vehicle}>
                      {vehicle.vehicleNumber} |
                      {vehicle.startStateName}:{vehicle.startCityName} |
                      {vehicle.endStateName}:{vehicle.endCityName} |
                      {vehicle.capacity} Tonne |
                      {vehicle.statusName} |
                      </option>);
                  })
                }
              </select>
            </Row>
            <Row className="row-container">
              <select id="currentState" style={style.dropdownStyle} onChange={() => this.resetCitiesAsPerSelection(document.getElementById('currentState'),document.getElementById('currentCity'))}>
                <option>Select current state</option>
                {
                  this.props.statesOrder.map((option) =>
                  {
                    return (<option key={option} value={option}>{option}</option>);
                  })
                }
              </select>
            </Row>
            <Row className="row-container">
              <select id="currentCity" style={style.dropdownStyle}>
                <option>Select current city</option>
              </select>
            </Row>
            <Row className="row-container">
              <select id="vehicleStatus" style={style.dropdownStyle}>
                <option>Select vehicle's new status</option>
                {
                  this.props.vehicleStatus.map((option) =>
                  {
                    return (<option key={option} value={option}>{option}</option>);
                  })
                }
              </select>
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onUpdateVehicleStatusClick.bind(this)}>Update Vehicle Status</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default UpdateVehicleStatus;

UpdateVehicleStatus.propTypes = {
  stateCities: PropTypes.object.isRequired,
  statesOrder: PropTypes.array.isRequired,
  getMyVehicles: PropTypes.func.isRequired,
  vehicles: PropTypes.array.isRequired,
  vehicleStatus: PropTypes.array.isRequired,
  userPhoneNumber: PropTypes.string.isRequired,
  onUpdateVehicleStatusClick: PropTypes.func.isRequired,
};

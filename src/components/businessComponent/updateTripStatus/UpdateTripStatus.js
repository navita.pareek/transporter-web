import React, {PropTypes} from 'react';
import {Button} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class UpdateTripStatus extends React.Component {

  componentDidMount() {
    this.props.getMyTrips(this.props.userPhoneNumber);
  }

  onUpdateTripStatusClick(){
    this.updateFormLabelText();
    if(this.areRequiredEntriesFilled() && this.areRequiredEntriesValid()){
      this.props.onUpdateTripStatusClick(
        this.props.userPhoneNumber,
        this.props.trips[document.getElementById('trip').selectedIndex - 1],
        document.getElementById('tripStatus').selectedIndex,
        document.getElementById('trip').selectedIndex - 1,

      );
    }
  }

  updateFormLabelText(){
    let title = document.getElementById('pageTitle');
    title.innerHTML = "Change the below fields to proceed update.";
    title.style["color"] = "rgb(51, 51, 51)";
  }

  areRequiredEntriesFilled(){
    if ((document.getElementById('trip').selectedIndex == 0) ||
    (document.getElementById('tripStatus').selectedIndex == 0)){
      let title = document.getElementById('pageTitle');
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  areRequiredEntriesValid(){
    if ((document.getElementById('tripStatus').selectedIndex) ==
      (this.props.trips[document.getElementById('trip').selectedIndex - 1].status)){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "New status selected is same as current status.";
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  render(){
    return (
      <div>
        <h2 style={style.titleStyle}>Update Trip Status</h2>
        <p style={style.textStyle} id="pageTitle">
          Change the below fields to proceed update.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <select id="trip" style={style.dropdownStyle}>
                <option>Select trip to update</option>
                {
                  this.props.trips.map((trip) =>
                  {
                    return (<option key={this.props.trips.indexOf(trip)} value={trip}>
                      {trip.startDate} |
                      {trip.startStateName}:{trip.startCityName} |
                      {trip.endStateName}:{trip.endCityName} |
                      {trip.vehicleCapacityRequired} Tonne |
                      {trip.statusName} |
                      </option>);
                  })
                }
              </select>
            </Row>
            <Row className="row-container">
              <select id="tripStatus" style={style.dropdownStyle}>
                <option>Select new trip status</option>
                {
                  this.props.tripStatus.map((option) =>
                  {
                    return (<option key={option} value={option}>{option}</option>);
                  })
                }
              </select>
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onUpdateTripStatusClick.bind(this)}>Update Trip Status</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default UpdateTripStatus;

UpdateTripStatus.propTypes = {
  getMyTrips: PropTypes.func.isRequired,
  trips: PropTypes.array.isRequired,
  tripStatus: PropTypes.array.isRequired,
  userPhoneNumber: PropTypes.string.isRequired,
  onUpdateTripStatusClick: PropTypes.func.isRequired,
};

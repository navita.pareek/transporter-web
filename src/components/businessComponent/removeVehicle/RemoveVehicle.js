import React, {PropTypes} from 'react';
import {Button, FormControl} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class RemoveVehicle extends React.Component {

  componentDidMount() {
    this.vehicleNumber.value;
  }

  onRemoveVehicleClick(){
    this.updateFormLabelText();
    if(this.areRequiredEntriesFilled() && this.areRequiredEntriesValid()){
      this.props.onRemoveVehicleClick(
        this.props.userPhoneNumber,
        this.vehicleNumber.value
      );
    }
  }

  updateFormLabelText(){
    let title = document.getElementById('pageTitle');
    title.innerHTML = "Fill the below information to proceed.";
    title.style["color"] = "rgb(51, 51, 51)";
  }

  areRequiredEntriesFilled(){
    if ((this.vehicleNumber.value.trim().length == 0)){
      let title = document.getElementById('pageTitle');
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  areRequiredEntriesValid() {
    return (this.isValidVehicleNumber())
  }

  isValidVehicleNumber(){
    if(this.vehicleNumber.value.trim().length < 3){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Enter valid vehicle number.";
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  render(){
    return (
      <div>
        <h2 style={style.titleStyle}>Remove Vehicle</h2>
        <p style={style.textStyle} id="pageTitle">
          Fill the below information to proceed.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <FormControl
                style={style.dashboardTextboxStyle}
                componentClass="input"
                inputRef={node => this.vehicleNumber = node}
                type="text"
                placeholder="Enter vehicle number to remove"
              />
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onRemoveVehicleClick.bind(this)}>Remove Vehicle</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default RemoveVehicle;

RemoveVehicle.propTypes = {
  userPhoneNumber: PropTypes.string.isRequired,
  onRemoveVehicleClick: PropTypes.func.isRequired,
};

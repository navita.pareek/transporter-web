import React, {PropTypes} from 'react';
import {Button, FormControl, Checkbox} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class AddManpower extends React.Component {

  componentDidMount() {
    this.newMemberPhoneNumber.value;
  }

  onAddManpowerClick(){
    this.updateFormLabelText();
    if(this.areRequiredEntriesFilled() && this.areRequiredEntriesValid()){
      this.props.onAddManpowerClick(this.props.userPhoneNumber,
        this.newMemberPhoneNumber.value,
        document.getElementById('IsEmpowered').checked,
      );
    }
  }

  updateFormLabelText(){
    let title = document.getElementById('pageTitle');
    title.innerHTML = "Fill the below information to proceed.";
    title.style["color"] = "rgb(51, 51, 51)";
  }

  areRequiredEntriesFilled(){
    if ((this.newMemberPhoneNumber.value.trim().length == 0)){
      let title = document.getElementById('pageTitle');
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  areRequiredEntriesValid() {
    return (this.isValidPhoneNumber())
  }

  isValidPhoneNumber(){
    if(this.newMemberPhoneNumber.value.trim() == this.props.userPhoneNumber){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Provide other phone number.";
      title.style["color"] = "red";
      return false;
    }

    if(this.newMemberPhoneNumber.value.trim().length < 6){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Enter valid phone number.";
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  render(){
    return (
      <div>
        <h2 style={style.titleStyle}>Add Manpower</h2>
        <p style={style.textStyle} id="pageTitle">
          Fill the below information to proceed.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <FormControl
                style={style.dashboardTextboxStyle}
                componentClass="input"
                inputRef={node => this.newMemberPhoneNumber = node}
                type="text"
                placeholder="Enter new member phone number"
              />
            </Row>
            <Row className="row-container" >
              <Checkbox id="IsEmpowered" style={style.dashboardCheckboxStyle}>New member is empowered to add more members.</Checkbox>
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onAddManpowerClick.bind(this)}>Add Manpower</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default AddManpower;

AddManpower.propTypes = {
  userPhoneNumber: PropTypes.string.isRequired,
  onAddManpowerClick: PropTypes.func.isRequired,
};

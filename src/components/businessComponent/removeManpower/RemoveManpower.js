import React, {PropTypes} from 'react';
import {Button, FormControl} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class RemoveManpower extends React.Component {

  componentDidMount() {
    this.newManpowerPhoneNumber.value;
  }

  onRemoveManpowerClick(){
    this.updateFormLabelText();
    if(this.areRequiredEntriesFilled() && this.areRequiredEntriesValid()){
      this.props.onRemoveManpowerClick(
        this.props.userPhoneNumber,
        this.newManpowerPhoneNumber.value
      );
    }
  }

  updateFormLabelText(){
    let title = document.getElementById('pageTitle');
    title.innerHTML = "Fill the below information to proceed.";
    title.style["color"] = "rgb(51, 51, 51)";
  }

  areRequiredEntriesFilled(){
    if ((this.newManpowerPhoneNumber.value.trim().length == 0)){
      let title = document.getElementById('pageTitle');
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  areRequiredEntriesValid() {
    return (this.isValidnewManpowerPhoneNumber())
  }

  isValidnewManpowerPhoneNumber(){
    if(this.newManpowerPhoneNumber.value.trim() == this.props.userPhoneNumber){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Provide other phone number.";
      title.style["color"] = "red";
      return false;
    }

    if(this.newManpowerPhoneNumber.value.trim().length < 6){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Enter valid phone number.";
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  render(){
    return (
      <div>
        <h2 style={style.titleStyle}>Remove Manpower</h2>
        <p style={style.textStyle} id="pageTitle">
          Fill the below information to proceed.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <FormControl
                style={style.dashboardTextboxStyle}
                componentClass="input"
                inputRef={node => this.newManpowerPhoneNumber = node}
                type="text"
                placeholder="Enter member's phone number to remove"
              />
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onRemoveManpowerClick.bind(this)}>Remove Manpower</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default RemoveManpower;

RemoveManpower.propTypes = {
  userPhoneNumber: PropTypes.string.isRequired,
  onRemoveManpowerClick: PropTypes.func.isRequired,
};

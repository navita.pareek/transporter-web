import React, {PropTypes} from 'react';
import {Button, FormControl, Checkbox} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class SearchVehicle extends React.Component {

  componentDidMount() {
    this.startDate.value;
    this.vehicleCapacityRequired.value;
  }

  resetCitiesAsPerSelection(states,cities) {
    cities.selectedIndex = 0;

    while (cities.options.length > 1) {
      cities.remove(0);
    }

    if(states.selectedIndex == 0)
      return;

    for (let i = 0; i < this.props.stateCities[states.options[states.selectedIndex].text].length; i++) {
      this.createOption(cities, this.props.stateCities[states.options[states.selectedIndex].text][i], i+1);
    }
  }

  createOption(ddl, text, index) {
    var opt = document.createElement('option');
    opt.text = text;
    ddl.options.add(opt, index);
  }

  onSearchVehicleClick(){
    this.updateFormLabelText();
    if(this.areRequiredEntriesFilled() && this.areRequiredEntriesValid()){
      this.props.onSearchVehicleClick(this.props.userPhoneNumber,
        this.startDate.value,
        document.getElementById('startState').selectedIndex,
        document.getElementById('startCity').selectedIndex,
        document.getElementById('endState').selectedIndex,
        document.getElementById('endCity').selectedIndex,
        this.vehicleCapacityRequired.value,
        document.getElementById('IsInvisible').checked
      );
    }
  }

  updateFormLabelText(){
    let title = document.getElementById('pageTitle');
    title.innerHTML = "Fill the below information to proceed.";
    title.style["color"] = "rgb(51, 51, 51)";
  }

  areRequiredEntriesFilled(){
    if ((this.startDate.value.trim().length == 0) ||
    (document.getElementById('startState').selectedIndex == 0) ||
    (this.vehicleCapacityRequired.value.trim().length == 0)){
      let title = document.getElementById('pageTitle');
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  areRequiredEntriesValid() {
    return (this.isValidStartDate() &&
    this.isValidVehicleCapacity())
  }

  isValidVehicleCapacity(){
    if(this.vehicleCapacityRequired.value <= 0){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Check vehicle capacity.";
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  isValidStartDate(){
    if(!this.isValidDateFormat() || !this.isValidDateRange()){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Enter valid date in dd/mm/yyyy format";
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  isValidDateRange() {
    let data = this.startDate.value.split("/");
    let today = new Date();

    return ((data[2] - today.getFullYear() == 1)
      || (data[2] - today.getFullYear() == 0 && data[1] - today.getMonth() > 1)
      || (data[2] - today.getFullYear() == 0 && data[1] - today.getMonth() == 1 && data[0] - today.getDate() >= 0));
  }

  isValidDateFormat(){
    let data = this.startDate.value.split("/");

    if(isNaN(data[0]) || isNaN(data[1]) || isNaN(data[2])){
      return false;
    }

    if ((data[2].length != 4) || (data[1].length != 2) || (data[0].length != 2)) {
      return false;
    }
    return true;
  }

  render(){
    return (
      <div>
        <h2 style={style.titleStyle}>Search Vehicle</h2>
        <p style={style.textStyle} id="pageTitle">
          Fill the below information to proceed.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <FormControl
                style={style.dashboardTextboxStyle}
                componentClass="input"
                inputRef={node => this.startDate = node}
                type="text"
                placeholder="Trip Start date in dd/mm/yyyy"
              />
            </Row>
            <Row className="row-container">
              <select id="startState" style={style.dropdownStyle} onChange={() => this.resetCitiesAsPerSelection(document.getElementById('startState'),document.getElementById('startCity'))}>
                <option>Select Start State</option>
                {
                  this.props.statesOrder.map((option) =>
                  {
                    return (<option key={option} value={option}>{option}</option>);
                  })
                }
              </select>
            </Row>
            <Row className="row-container">
              <select id="startCity" style={style.dropdownStyle}>
                <option>Select Start City</option>
              </select>
            </Row>
            <Row className="row-container">
              <FormControl
                style={style.dashboardTextboxStyle}
                componentClass="input"
                inputRef={node => this.vehicleCapacityRequired = node}
                type="number"
                placeholder="Vehicle Capacity required in tonne"
              />
            </Row>
            <Row className="row-container">
              <select id="endState" style={style.dropdownStyle} onChange={() => this.resetCitiesAsPerSelection(document.getElementById('endState'),document.getElementById('endCity'))}>
                <option>Select End State</option>
                {
                  this.props.statesOrder.map((option) =>
                  {
                    return (<option key={option} value={option}>{option}</option>);
                  })
                }
              </select>
            </Row>
            <Row className="row-container">
              <select id="endCity" style={style.dropdownStyle}>
                <option>Select Start City</option>
              </select>
            </Row>
            <Row className="row-container" >
              <Checkbox id="IsInvisible" style={style.dashboardCheckboxStyle} defaultChecked="true">Contact me if match found later.</Checkbox>
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onSearchVehicleClick.bind(this)}>Search Vehicle</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default SearchVehicle;

SearchVehicle.propTypes = {
  stateCities: PropTypes.object.isRequired,
  statesOrder: PropTypes.array.isRequired,
  userPhoneNumber: PropTypes.string.isRequired,
  onSearchVehicleClick: PropTypes.func.isRequired,
};

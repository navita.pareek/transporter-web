import React, {PropTypes} from 'react';
import {Button, FormControl, Checkbox} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class AddCompany extends React.Component {

  componentDidMount() {
    this.companyName.value;
  }

  onAddCompanyClick(){
    this.updateFormLabelText();
    if(this.areRequiredEntriesFilled() && this.areRequiredEntriesValid()){
      this.props.onAddCompanyClick(this.props.userPhoneNumber,
        this.companyName.value);
    }
  }

  updateFormLabelText(){
    let title = document.getElementById('pageTitle');
    title.innerHTML = "Fill the below information to proceed.";
    title.style["color"] = "rgb(51, 51, 51)";
  }

  areRequiredEntriesFilled(){
    if ((this.companyName.value.trim().length == 0)){
      let title = document.getElementById('pageTitle');
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  areRequiredEntriesValid() {
    return (this.isValidCompanyName())
  }

  isValidCompanyName(){
    if(this.companyName.value.trim().length < 4){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Enter valid company name.";
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  render(){
    return (
      <div>
        <h2 style={style.titleStyle}>Add Company</h2>
        <p style={style.textStyle} id="pageTitle">
          Fill the below information to proceed.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <FormControl
                style={style.dashboardTextboxStyle}
                componentClass="input"
                inputRef={node => this.companyName = node}
                type="text"
                placeholder="Enter company name"
              />
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onAddCompanyClick.bind(this)}>Add Company</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default AddCompany;

AddCompany.propTypes = {
  userPhoneNumber: PropTypes.string.isRequired,
  onAddCompanyClick: PropTypes.func.isRequired,
};

import React, {PropTypes} from 'react';
import {Button, FormControl, Checkbox} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class MyTrips extends React.Component {

  componentDidMount() {
    this.props.getMyTrips(this.props.userPhoneNumber);
  }

  render(){
    if(this.props.trips.length == 0){
      return (<div>
        <h2 style={style.titleStyle}>My Trips</h2>
        <p style={style.textStyle} id="pageTitle">
          No trip to display..
        </p>
      </div>);
    }

    return (
      <div>
        <h2 style={style.titleStyle}>My Trips</h2>
        <p style={style.textStyle} id="pageTitle">
          {this.props.trips.length} trips to show.
          <br/>
          <span key="End">-------------------------------------------------------------</span>
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            {
              this.props.trips.map((option, index) =>
              {
                return (<Row className="row-container" key={index}>
                  <span key="capacity" value={option.capacity}>Weight: {option.vehicleCapacityRequired} Tonne</span>
                  <br/>
                  <span key="startStateName" value={option.startStateName}>StartLocation: {option.startStateName}:{option.startCityName}</span>
                  <br/>
                  <span key="endStateName" value={option.endStateName}>EndLocation: {option.endStateName}:{option.endCityName}</span>
                  <br/>
                  <span key="status" value={option.statusName}>Status: {option.statusName}</span>
                  <br/>
                  <span key="End">-------------------------------------------------------------</span>
                </Row>);
              })
            }
          </Grid>
        </div>
      </div>
    );
  }
}

export default MyTrips;

MyTrips.propTypes = {
  userPhoneNumber: PropTypes.string.isRequired,
  getMyTrips: PropTypes.func.isRequired,
  trips: PropTypes.array.isRequired
};

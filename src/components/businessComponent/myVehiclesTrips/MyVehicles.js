import React, {PropTypes} from 'react';
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";
class MyVehicles extends React.Component {

  componentDidMount() {
    this.props.getMyVehicles(this.props.userPhoneNumber);
  }

  render(){
    if(this.props.vehicles.length == 0){
      return (<div>
        <h2 style={style.titleStyle}>My Vehicles</h2>
        <p style={style.textStyle} id="pageTitle">
          No vehicle to display..
        </p>
      </div>);
    }

    return (
      <div>
        <h2 style={style.titleStyle}>My Vehicles</h2>
        <p style={style.textStyle} id="pageTitle">
          {this.props.vehicles.length} vehicles to show.
          <br/>
          <span key="End">-------------------------------------------------------------</span>
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            {
              this.props.vehicles.map((option) =>
              {
                return (<Row className="row-container" key={option.vehicleNumber}>
                  <span key={option.vehicleNumber} value={option.vehicleNumber}>Vehicle Number: {option.vehicleNumber}</span>
                  <br/>
                  <span key="capacity" value={option.capacity}>Capacity: {option.capacity} Tonne</span>
                  <br/>
                  <span key="startStateName" value={option.startStateName}>StartLocation: {option.startStateName}:{option.startCityName}</span>
                  <br/>
                  <span key="endStateName" value={option.endStateName}>EndLocation: {option.endStateName}:{option.endCityName}</span>
                  <br/>
                  <span key={option.statusName} value={option.statusName}>Status: {option.statusName}</span>
                  <br/>
                  <span key="End">-------------------------------------------------------------</span>
                </Row>);
              })
            }
          </Grid>
        </div>
      </div>
    );
  }
}

export default MyVehicles;

MyVehicles.propTypes = {
  userPhoneNumber: PropTypes.string.isRequired,
  getMyVehicles: PropTypes.func.isRequired,
  vehicles: PropTypes.array.isRequired
};

import React, {PropTypes} from 'react';
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class SearchResult extends React.Component {


  render(){
    if(this.props.result.length == 0){
      return (<div>
        <h2 style={style.titleStyle}></h2>
        <p style={style.textStyle} id="pageTitle">
          No Result to display..
        </p>
      </div>);
    }

    return (
      <div>
        <h2 style={style.titleStyle}>Result</h2>
        <p style={style.textStyle} id="pageTitle">
          We have found match as per your requirement. You may contact the below numbers.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
          {
            this.props.result.map((option) =>
            {
              return (<Row className="row-container" key={option}><span key={option} value={option}>{option}</span></Row>);
            })
          }
          </Grid>
        </div>
      </div>
    );
  }
}

export default SearchResult;

SearchResult.propTypes = {
  result: PropTypes.array.isRequired
};

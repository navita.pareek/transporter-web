import React, {PropTypes} from 'react';
import {Button, FormControl, Checkbox} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as style from "../../../common/style";

class AddVehicle extends React.Component {

  componentDidMount() {
    this.vehicleNumber.value;
    this.vehicleCapacity.value;
  }

  resetCitiesAsPerSelection(states,cities) {
    cities.selectedIndex = 0;

    while (cities.options.length > 1) {
      cities.remove(0);
    }

    if(states.selectedIndex == 0)
      return;

    for (let i = 0; i < this.props.stateCities[states.options[states.selectedIndex].text].length; i++) {
      this.createOption(cities, this.props.stateCities[states.options[states.selectedIndex].text][i], i+1);
    }
  }

  createOption(ddl, text, index) {
    var opt = document.createElement('option');
    opt.text = text;
    ddl.options.add(opt, index);
  }

  onAddVehicleClick(){
    this.updateFormLabelText();
    if(this.areRequiredEntriesFilled() && this.areRequiredEntriesValid()){
      this.props.onAddVehicleClick(this.props.userPhoneNumber,
        this.vehicleNumber.value,
        document.getElementById('startState').selectedIndex,
        document.getElementById('startCity').selectedIndex,
        document.getElementById('endState').selectedIndex,
        document.getElementById('endCity').selectedIndex,
        this.vehicleCapacity.value,
        document.getElementById('IsLinkedToCompany').checked,
      );
    }
  }

  updateFormLabelText(){
    let title = document.getElementById('pageTitle');
    title.innerHTML = "Fill the below information to proceed.";
    title.style["color"] = "rgb(51, 51, 51)";
  }

  areRequiredEntriesFilled(){
    if ((this.vehicleNumber.value.trim().length == 0) ||
    (this.vehicleCapacity.value.trim().length == 0)){
      let title = document.getElementById('pageTitle');
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  areRequiredEntriesValid() {
    return (this.isValidVehicleNumber() &&
    this.isValidVehicleCapacity())
  }

  isValidVehicleCapacity(){
    if(this.vehicleCapacity.value <= 0){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Check vehicle capacity.";
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  isValidVehicleNumber(){
    if(this.vehicleNumber.value.trim().length < 3){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Enter valid vehicle number.";
      title.style["color"] = "red";
      return false;
    }

    return true;
  }

  render(){
    return (
      <div>
        <h2 style={style.titleStyle}>Add Vehicle</h2>
        <p style={style.textStyle} id="pageTitle">
          Fill the below information to proceed.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <FormControl
                style={style.dashboardTextboxStyle}
                componentClass="input"
                inputRef={node => this.vehicleNumber = node}
                type="text"
                placeholder="Enter vehicle number"
              />
            </Row>
            <Row className="row-container">
              <FormControl
                style={style.dashboardTextboxStyle}
                componentClass="input"
                inputRef={node => this.vehicleCapacity = node}
                type="number"
                placeholder="Enter vehicle capacity in tonne"
              />
            </Row>
            <Row className="row-container" >
              <Checkbox id="IsLinkedToCompany" style={style.dashboardCheckboxStyle}>Link this vehicle to my company.</Checkbox>
            </Row>
            <Row className="row-container">
              <select id="startState" style={style.dropdownStyle} onChange={() => this.resetCitiesAsPerSelection(document.getElementById('startState'),document.getElementById('startCity'))}>
                <option>Select preferred start state</option>
                {
                  this.props.statesOrder.map((option) =>
                  {
                    return (<option key={option} value={option}>{option}</option>);
                  })
                }
              </select>
            </Row>
            <Row className="row-container">
              <select id="startCity" style={style.dropdownStyle}>
                <option>Select preferred start city</option>
              </select>
            </Row>
            <Row className="row-container">
              <select id="endState" style={style.dropdownStyle} onChange={() => this.resetCitiesAsPerSelection(document.getElementById('endState'),document.getElementById('endCity'))}>
                <option>Select preferred end state</option>
                {
                  this.props.statesOrder.map((option) =>
                  {
                    return (<option key={option} value={option}>{option}</option>);
                  })
                }
              </select>
            </Row>
            <Row className="row-container">
              <select id="endCity" style={style.dropdownStyle}>
                <option>Select preferred end city</option>
              </select>
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onAddVehicleClick.bind(this)}>Add Vehicle</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default AddVehicle;

AddVehicle.propTypes = {
  stateCities: PropTypes.object.isRequired,
  statesOrder: PropTypes.array.isRequired,
  vehicleStatus: PropTypes.array.isRequired,
  userPhoneNumber: PropTypes.string.isRequired,
  onAddVehicleClick: PropTypes.func.isRequired,
};

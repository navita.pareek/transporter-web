export function getStateIndexByLocation(location) {
  const stateCity = location.split(':');
  if(stateCity.length > 0)
    return stateCity[0];
  return 0;
}

export function getCityIndexByLocation(location) {
  const stateCity = location.split(':');
  if(stateCity.length > 1)
    return stateCity[1];
  return 0;
}

export function getStateByLocation(location, statesOrder) {
  const stateIndex = location.substring(0, location.indexOf(':')) - 1;
  if(stateIndex > -1){
    return statesOrder[stateIndex];
  }
  return "";
}

export function getCityByLocation(location, statesOrder, statesCities) {
  const stateIndex = location.substring(0, location.indexOf(':')) - 1;
  const cityIndex = location.substring(location.indexOf(':') + 1) - 1;
  if(stateIndex > -1){
    const state = statesOrder[stateIndex];
    if(cityIndex > - 1 && state.length > 0){
      return statesCities[state][cityIndex];
    }
  }
  return "";
}

export function getVehicleStatusByIndex(index, vehicleStatus) {
  if(index < 1 || index > vehicleStatus.length)
    return "";

  return vehicleStatus[index - 1];
}

export function getTripStatusByIndex(index, tripStatus) {
  if(index < 1 || index > tripStatus.length)
    return "";

  return tripStatus[index - 1];
}

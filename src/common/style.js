let primaryDarkGreenColor = '#4f9e9e'
let primaryDaryBlueColor = '#3f51b5'

export const tableStyle = {
  color: 'black',
  width: '100%',
  border: '1px solid ' + primaryDaryBlueColor
};

export const rowStyle = {
  display: 'flex',
  width:'100%'
};

export const cellStyle = {
  padding: '10px',
  border: '1px outset ' + primaryDaryBlueColor,
  width: '33%'
};

export const dashboardButtonsListCellStyle = {
  padding: '5px',
  border: '1px outset ' + primaryDaryBlueColor,
  width: '15%'
};

export const dashboardContentCellStyle = {
  padding: '10px',
  border: '1px outset ' + primaryDaryBlueColor,
  width: '40%'
};

export const dashboardContentFullCellStyle = {
  padding: '10px',
  border: '1px outset ' + primaryDaryBlueColor,
  width: '85%'
};

export const dashboardResultCellStyle = {
  padding: '10px',
  border: '1px outset ' + primaryDaryBlueColor,
  width: '40%'
};

export const textStyle = {
  color: '#333',
};

export const titleStyle = {
  color: primaryDarkGreenColor,
};

export const buttonStyle = {
  background: primaryDarkGreenColor,
  border:'none',
  padding: '5px',
  display: 'inline-block',
  margin:'5px 0 0 0'
};

export const buttonDashboardStyle = {
  background: primaryDarkGreenColor,
  border:'none',
  padding: '5px',
  display: 'inline-block',
  margin:'4px 0 0 0',
  width:'100%',
  cursor: 'pointer'
};

export const buttonDashboardSelectedStyle = {
  background: primaryDaryBlueColor,
  border:'none',
  padding: '5px',
  display: 'inline-block',
  margin:'4px 0 0 0',
  width:'100%',
  color:'#fff'
};

export const textboxStyle = {
  width: '50%',
  margin: '5px'
};

export const dropdownStyle = {
  width: '62%',
  margin: '5px 0px 5px'
};

export const dashboardTextboxStyle = {
  width: '60%',
  margin: '5px 0px 5px'
};

export const dashboardCheckboxStyle = {
  width: '80%',
  margin: '5px 0px 5px',
  color: '#333',
};

import React, {PropTypes} from 'react';
import {Button, FormControl} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as msg from '../../constants/CoreErrors';
import * as style from "../../common/style";


class Login extends React.Component {

  componentDidMount() {
    this.phoneNumber.value;
    this.password.value;
  }

  onLoginClick(){
    if(this.isValidPhoneNumber(this.phoneNumber.value) && this.isValidPassword(this.password.value)){
      this.props.onLoginClick(this.phoneNumber.value, this.password.value);
    }else{
      alert(msg.INVALID_INFORMATION_PROVIDED);
    }
  }

  isValidPhoneNumber(phoneNumber){
    return phoneNumber.trim().length == 10 && (/^\d+$/.test(phoneNumber));
  }

  isValidPassword(password){
    return password.length == 6;
  }

  render(){

    return (
      <div>
        <h2 style={style.titleStyle}>Login</h2>
        <p style={style.textStyle}>
          Provide credentials, if already Registered.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <FormControl
                style={style.textboxStyle}
                componentClass="input"
                inputRef={node => this.phoneNumber = node}
                type="text"
                placeholder="Your phone number"
              />
            </Row>
            <Row className="row-container">
              <FormControl
                style={style.textboxStyle}
                key="password"
                inputRef={node => this.password = node}
                type="password"
                placeholder="PIN"
              />
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onLoginClick.bind(this)}>Login</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default Login;

Login.propTypes = {
  onLoginClick: PropTypes.func.isRequired
};

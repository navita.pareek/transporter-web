import Login from './Login';
import Register from './Register';
import logo from '../../resource/logo.png';
import * as style from "../../common/style";
import React, {Component, PropTypes} from 'react';
import s from './styles.css';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/homePageActions';
import Header from "../../components/baseComponents/Layout/Header";

class HomePage extends Component {

  componentWillMount(){
    this.props.actions.logoutUser();
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={s.container}>
        <Header/>
        <table style={style.tableStyle}>
          <tbody style={style.rowStyle} >
            <tr style={style.rowStyle} >
              <td style={style.cellStyle}>
                <div style={style.textStyle}>
                  <img src={logo} />
                </div>
              </td>
              <td style={style.cellStyle}><div>
                <Login onLoginClick={this.props.actions.onLoginClicked}/>
              </div></td>
              <td style={style.cellStyle}><div>
                <Register onRegisterClick={this.props.actions.onRegisterClicked}/>
              </div></td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

HomePage.propTypes = {
  actions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);

import React, {PropTypes} from 'react';
import {Button, FormControl} from "react-bootstrap";
import {Grid, Row} from 'react-bootstrap';
import * as msg from '../../constants/CoreErrors';
import * as style from "../../common/style";

class Register extends React.Component {

  componentDidMount() {
    this.phoneNumber.value;
    this.password.value;
    this.dateOfBirth.value;
    this.confirmPassword.value;
  }

  onRegisterClick(){
    this.updateFormLabelText();
    if(this.allRequiredFieldsProvided() && this.isValidPhoneNumber(this.phoneNumber.value) && this.isValidDateOfBirth(this.dateOfBirth.value) && this.isValidPassword(this.password.value, this.confirmPassword.value)){
      this.props.onRegisterClick(this.phoneNumber.value, this.password.value, this.dateOfBirth.value, "ENGLISH");
    }
  }

  updateFormLabelText(){
    let title = document.getElementById('pageTitle');
    title.innerHTML = "Provide below details to be part of Transporter Network.";
    title.style["color"] = "rgb(51, 51, 51)";
  }

  allRequiredFieldsProvided(){
    if(this.phoneNumber.value.trim().length == 0
      || this.password.value.trim().length == 0
      || this.confirmPassword.value.trim().length == 0
      || this.dateOfBirth.value.trim().length == 0){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Provide below details to be part of Transporter Network.";
      title.style["color"] = "red";
      return false;
    }
    return true;
  }

  isValidPhoneNumber(phoneNumber){
    if(phoneNumber.trim().length != 10 || !(/^\d+$/.test(phoneNumber))){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Provide valid phone number.";
      title.style["color"] = "red";
      return false;
    }
    return true;
  }

  isValidPassword(password, confirmPassword){
    if(password.length != 6 || password != confirmPassword){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Check if provided password and matching confirm password of length 6.";
      title.style["color"] = "red";
      return false;
    }
    return true;
  }

  isValidDateOfBirth(dateOfBirth) {
    return this.isValidDateFormat(dateOfBirth) && this.isValidDateRange(dateOfBirth);
  }

  isValidDateFormat(dateOfBirth){
    let data = dateOfBirth.split("/");

    if (isNaN(data[0]) || isNaN(data[1]) || isNaN(data[2]) ||
      (data[2].length != 4) || (data[1].length != 2) || (data[0].length != 2) ||
      !(/^\d+$/.test(data[2])) || !(/^\d+$/.test(data[1])) || !(/^\d+$/.test(data[1]))) {
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Provided date of birth in dd/mm/yyyy format.";
      title.style["color"] = "red";
      return false;
    }
    return true;
  }

  isValidDateRange(dateOfBirth) {
    let today = new Date();
    let birthDate = new Date(dateOfBirth);
    let age = today.getFullYear() - birthDate.getFullYear();
    if(age < 15 || age > 80){
      let title = document.getElementById('pageTitle');
      title.innerHTML = "Valid age to join Transport Network is 15 to 80 years.";
      title.style["color"] = "red";
      return false;
    }
    return true;
  }

  render() {

    return (
      <div>
        <h2 style={style.titleStyle}>Register</h2>
        <p id="pageTitle" style={style.textStyle}>
          Provide below details to be part of Transporter Network.
        </p>
        <div className="content-container">
          <Grid fluid className="home-page">
            <Row className="row-container">
              <FormControl
                style={style.textboxStyle}
                componentClass="input"
                inputRef={node => this.phoneNumber = node}
                type="text"
                placeholder="Your phone number"
              />
            </Row>
            <Row className="row-container">
              <FormControl
                style={style.textboxStyle}
                componentClass="input"
                inputRef={node => this.dateOfBirth = node}
                type="text"
                placeholder="Date of birth in dd/mm/yyyy"
              />
            </Row>
            <Row className="row-container">
              <FormControl
                style={style.textboxStyle}
                key="password"
                inputRef={node => this.password = node}
                type="password"
                placeholder="6 digit PIN"
              />
            </Row>
            <Row className="row-container">
              <FormControl
                style={style.textboxStyle}
                key="password"
                inputRef={node => this.confirmPassword = node}
                type="password"
                placeholder="Confirm above PIN"
              />
            </Row>
            <Row className="row-container">
              <div className="btn-panel right">
                <Button style={style.buttonStyle} onClick={this.onRegisterClick.bind(this)}>Register</Button>
              </div>
            </Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default Register;

Register.propTypes = {
  onRegisterClick: PropTypes.func.isRequired
};

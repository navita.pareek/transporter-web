import {Button} from "react-bootstrap";
import * as style from "../../../common/style";
import React, {Component, PropTypes} from 'react';
import s from '../styles.css';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../../actions/DashboardActions';
import Header from "../../../components/baseComponents/Layout/Header";
import MyVehicles from "../../../components/businessComponent/myVehiclesTrips/MyVehicles";
import MyTrips from "../../../components/businessComponent/myVehiclesTrips/MyTrips";
import history from '../../../history';

class AddCompanyPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={s.container}>
        <Header/>
        <table style={style.tableStyle}>
          <tbody style={style.rowStyle} >
            <tr style={style.rowStyle} >
              <td style={style.dashboardButtonsListCellStyle}>
                <div style={style.textStyle}>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/searchVehicle" })}>Search Vehicle</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/searchLoad" })}>Search Load</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/addVehicle" })}>Add Vehicle</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/addCompany" })}>Add Company</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/addManpower" })}>Add Manpower</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/updateVehicleStatus" })}>Update Vehicle Status</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/updateTripStatus" })}>Update Trip Status</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardSelectedStyle}>My Vehicles/Trips</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/removeVehicle" })}>Remove Vehicle</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/removeManpower" })}>Remove Manpower</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/letsConnect" })}>Let's Connect</Button>
                  </div>
                </div>
              </td>
              <td style={style.dashboardContentCellStyle}>
                <div style={style.textStyle}>
                  <MyVehicles userPhoneNumber={this.props.user.userPhoneNumber}
                              getMyVehicles={this.props.actions.getMyVehicles}
                              vehicles={this.props.dashboard.vehicles}/>
                </div>
              </td>
              <td style={style.dashboardResultCellStyle}>
                <div style={style.textStyle}>
                  <MyTrips userPhoneNumber={this.props.user.userPhoneNumber}
                           getMyTrips={this.props.actions.getMyTrips}
                           trips={this.props.dashboard.trips}/>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

AddCompanyPage.propTypes = {
  actions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    user: state.home.user,
    dashboard: state.dashboard
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddCompanyPage);

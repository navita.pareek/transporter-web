import {Button} from "react-bootstrap";
import * as style from "../../../common/style";
import React, {Component, PropTypes} from 'react';
import s from '../styles.css';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../../actions/DashboardActions';
import Header from "../../../components/baseComponents/Layout/Header";
import LetsConnect from "../../../components/businessComponent/letsConnect/LetsConnect";
import history from '../../../history';

class LetsConnectPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={s.container}>
        <Header/>
        <table style={style.tableStyle}>
          <tbody style={style.rowStyle} >
            <tr style={style.rowStyle} >
              <td style={style.dashboardButtonsListCellStyle}>
                <div style={style.textStyle}>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/searchVehicle" })}>Search Vehicle</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/searchLoad" })}>Search Load</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/addVehicle" })}>Add Vehicle</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/addCompany" })}>Add Company</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/addManpower" })}>Add Manpower</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/updateVehicleStatus" })}>Update Vehicle Status</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/updateTripStatus" })}>Update Trip Status</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/myVehiclesTrips" })}>My Vehicles/Trips</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/removeVehicle" })}>Remove Vehicle</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/removeManpower" })}>Remove Manpower</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardSelectedStyle}>Let's Connect</Button>
                  </div>
                </div>
              </td>
              <td style={style.dashboardContentFullCellStyle}>
                <div style={style.textStyle}>
                  <LetsConnect userPhoneNumber={this.props.user.userPhoneNumber}
                               onLetsConnectClick={this.props.actions.onLetsConnectClick}/>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

LetsConnectPage.propTypes = {
  actions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    user: state.home.user,
    dashboard: state.dashboard
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LetsConnectPage);

import {Button} from "react-bootstrap";
import * as style from "../../../common/style";
import React, {Component, PropTypes} from 'react';
import s from '../styles.css';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../../actions/DashboardActions';
import Header from "../../../components/baseComponents/Layout/Header";
import AddManpower from "../../../components/businessComponent/addManpower/AddManpower";
import history from '../../../history';

class AddManpowerPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={s.container}>
        <Header/>
        <table style={style.tableStyle}>
          <tbody style={style.rowStyle} >
            <tr style={style.rowStyle} >
              <td style={style.dashboardButtonsListCellStyle}>
                <div style={style.textStyle}>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/searchVehicle" })}>Search Vehicle</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/searchLoad" })}>Search Load</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/addVehicle" })}>Add Vehicle</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/addCompany" })}>Add Company</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardSelectedStyle}>Add Manpower</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/updateVehicleStatus" })}>Update Vehicle Status</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/updateTripStatus" })}>Update Trip Status</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/myVehiclesTrips" })}>My Vehicles/Trips</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/removeVehicle" })}>Remove Vehicle</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/removeManpower" })}>Remove Manpower</Button>
                  </div>
                  <div className="btn-panel right">
                    <Button style={style.buttonDashboardStyle} onClick={() => history.push({ pathname: "/dashboard/letsConnect" })}>Let's Connect</Button>
                  </div>
                </div>
              </td>
              <td style={style.dashboardContentCellStyle}>
                <div style={style.textStyle}>
                  <AddManpower userPhoneNumber={this.props.user.userPhoneNumber}
                              onAddManpowerClick={this.props.actions.onAddManpowerClick}/>
                </div>
              </td>
              <td style={style.dashboardResultCellStyle}>
                <div style={style.textStyle}>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

AddManpowerPage.propTypes = {
  actions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    user: state.home.user,
    dashboard: state.dashboard
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddManpowerPage);

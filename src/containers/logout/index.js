import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/homePageActions';
import history from '../../history';

class LogoutPage extends Component {

  logoutUser(){
    this.props.actions.logoutUser();
  }

  constructor(props) {
    super(props);
  }

  render() {
    this.logoutUser();
    history.push({ pathname: "/" })
    return null;
  }
}

function mapStateToProps(state) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogoutPage);

import logo from '../../resource/logo.png';
import * as style from "../../common/style";
import React, {Component, PropTypes} from 'react';
import s from './styles.css';
import {connect} from 'react-redux';
import Header from "../../components/baseComponents/Layout/Header";

class AboutPage extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={s.container}>
        <Header/>
        <table style={style.tableStyle}>
          <tbody style={style.rowStyle} >
            <tr style={style.rowStyle} >
              <td style={style.cellStyle}>
                <div style={style.textStyle}>
                  <img src={logo} />
                </div>
              </td>
              <td style={style.dashboardContentFullCellStyle}>
                <h3 style={style.titleStyle}>
                  Welcome to Transporter Network.
                </h3>
                <span>Anyone can join transport network and use following benefits:</span>
                <br/>
                <span>1. Register as a user and check the availability of vehicles and loads, using "Search Vehicle" and "Search Load".</span>
                <br/>
                <span>2. Register your vehicle and get recognised by transporters, using "Add Vehicle". (No need to add company name for adding vehicles.)</span>
                <br/>
                <span>3. Add your company and other members of your business, using "Add Company" and "Add Manpower".</span>
                <br/>
                <span>4. Update trips and vehicles' information to get quick next availability, using "Update Trip Status" and "Update Vehicle Status".</span>
                <br/>
                <span>5. Have quick and easy access to all your vehicles and trips reports, using "My Vehicles/Trips".</span>
                <br/>
                <span>6. Connect to Transport Network Technical Team, using "Let's Connect", after login.</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AboutPage);

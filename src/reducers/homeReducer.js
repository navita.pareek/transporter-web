import * as types from "../constants/actionTypes";
import initialState from "./initialState";
import {fromJS} from "immutable";

export default function homeReducer(state = initialState.homeConfig, action) {

  switch (action.type) {

    case types.ON_LOGIN_CLICKED: {
      let response = JSON.parse(action.data);
      return fromJS(state).updateIn(['user', 'userPhoneNumber'], () => response.phoneNumber).toJS();
    }

    case types.ON_REGISTER_CLICKED:{
      return fromJS(state).updateIn(['user', 'userPhoneNumber'], () => action.data).toJS();
    }

    case types.CLEAR_USER_INFO: {
      return fromJS(state).updateIn([ 'user', 'userPhoneNumber'], () => "").toJS();
    }

    case types.FETCH_STATES_CITIES:{
      let interimArray = action.data.split(",");
      let stateCityMap = new Object();
      for(let index = 0; index < interimArray.length; index++){
        if(interimArray[index].length > 0){
          let temp = interimArray[index].split(":");
          if(temp[1] == "null") {
            stateCityMap[temp[0]] = [];
          }else{
            stateCityMap[temp[0]].push(temp[1]);
          }
        }
      }
      return fromJS(state).updateIn(['support', 'statesCities'], () => stateCityMap).toJS();
    }

    case types.SHOW_ERROR_MESSAGE: {
      alert(action.data);
      return state;
    }

    default:
      return state;
  }
}

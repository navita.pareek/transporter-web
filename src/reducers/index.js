import { combineReducers } from 'redux';
import home from "./homeReducer";
import dashboard from "./dashboardReducer";
import {routerReducer} from 'react-router-redux';

const rootReducer = combineReducers({
  home,
  dashboard,
  routing: routerReducer
});

export default rootReducer;

import * as types from "../constants/actionTypes";
import initialState from "./initialState";
import {fromJS} from "immutable";
import * as util from "../common/util";

export default function dashboardReducer(state = initialState.dashboardConfig, action) {

  switch (action.type) {

    case types.CLEAR_DASHBOARD_INFO: {
      const clearSearchVehicle = fromJS(state).updateIn(['searchVehicle'], () => []);
      const clearSearchLoad = fromJS(clearSearchVehicle).updateIn(['searchLoad'], () => []);
      const clearVehicles = fromJS(clearSearchLoad).updateIn(['vehicles'], () => []);
      const clearTrips = fromJS(clearVehicles).updateIn(['trips'], () => []);
      const clearCompany = fromJS(clearTrips).updateIn(['company'], () => "");
      return clearCompany.toJS();
    }

    case types.SEARCH_VEHICLE_CLICKED: {
      let response = JSON.parse(action.data).users;
      let resultInfo = response.map((result) => result.phoneNumber).filter((value, index, self) => self.indexOf(value) === index);
      if(resultInfo.length == 0){
        alert("No match found as per your requirement.");
      }
      return fromJS(state).updateIn(['searchVehicle'], () => resultInfo).toJS();
    }

    case types.SEARCH_LOAD_CLICKED: {
      let response = JSON.parse(action.data).users;
      let resultInfo = response.map((result) => result.phoneNumber).filter((value, index, self) => self.indexOf(value) === index);
      if(resultInfo.length == 0){
        alert("No match found as per your requirement.");
      }
      return fromJS(state).updateIn(['searchLoad'], () => resultInfo).toJS();
    }

    case types.ADD_VEHICLE_CLICKED: {
      let currentVehicles = fromJS(state.vehicles).toJS();
      currentVehicles.push(action.data);
      return fromJS(state).updateIn(['vehicles'], () => currentVehicles).toJS();
    }

    case types.ADD_COMPANY_CLICKED: {
      return fromJS(state).updateIn(['company'], () => action.data).toJS();
    }

    case types.ADD_MANPOWER_CLICKED: {
      return state;
    }

    case types.REMOVE_VEHICLE_CLICKED: {
      let currentVehicles = fromJS(state.vehicles).toJS();
      currentVehicles = currentVehicles.filter(function(vehicle) {
        return vehicle.vehicleNumber !== action.data
      })
      return fromJS(state).updateIn(['vehicles'], () => currentVehicles).toJS();
    }

    case types.REMOVE_MANPOWER_CLICKED: {
      return state;
    }

    case types.GET_MY_VEHICLES:{
      let response = JSON.parse(action.data).vehicles;
      let formattedResult = response.map(
        (vehicle) => {
          vehicle["startStateName"] = util.getStateByLocation(vehicle.startLocation, state.support.statesOrder);
          vehicle["startCityName"] = util.getCityByLocation(vehicle.startLocation, state.support.statesOrder, state.support.statesCities);
          vehicle["endStateName"] = util.getStateByLocation(vehicle.endLocation, state.support.statesOrder);
          vehicle["endCityName"] = util.getCityByLocation(vehicle.endLocation, state.support.statesOrder, state.support.statesCities);
          vehicle["statusName"] = util.getVehicleStatusByIndex(vehicle.status, state.support.vehicleStatus);
          return vehicle;}
      );
      return fromJS(state).updateIn(['vehicles'], () => formattedResult).toJS();
    }

    case types.GET_MY_TRIPS:{
      let response = JSON.parse(action.data).trips;
      let formattedResult = response.map(
        (trip) => {
          trip["startStateName"] = util.getStateByLocation(trip.startLocation, state.support.statesOrder);
          trip["startCityName"] = util.getCityByLocation(trip.startLocation, state.support.statesOrder, state.support.statesCities);
          trip["endStateName"] = util.getStateByLocation(trip.endLocation, state.support.statesOrder);
          trip["endCityName"] = util.getCityByLocation(trip.endLocation, state.support.statesOrder, state.support.statesCities);
          trip["statusName"] = util.getTripStatusByIndex(trip.status, state.support.tripStatus);
          return trip;}
      );
      return fromJS(state).updateIn(['trips'], () => formattedResult).toJS();
    }

    case types.UPDATE_TRIP_STATUS_CLICKED:{
      let currentTrips = fromJS(state.trips).toJS();
      let formattedResult = currentTrips.map(
        (trip, index) => {
          if(index == action.data.tripIndex){
            trip["status"] = action.data.newStatus;
            trip["statusName"] = util.getTripStatusByIndex(action.data.newStatus, state.support.tripStatus);
          }
          return trip;}
      );
      return fromJS(state).updateIn(['trips'], () => formattedResult).toJS();
    }

    case types.UPDATE_VEHICLE_STATUS_CLICKED:{
      let currentVehicles = fromJS(state.vehicles).toJS();
      let formattedResult = currentVehicles.map(
        (vehicle, index) => {
          if(index == action.data.vehicleIndex){
            vehicle["status"] = action.data.newStatus;
            vehicle["statusName"] = util.getVehicleStatusByIndex(action.data.newStatus, state.support.vehicleStatus);
            vehicle["startStateName"] = util.getStateByLocation(action.data.currentLocation, state.support.statesOrder);
            vehicle["startCityName"] = util.getCityByLocation(action.data.currentLocation, state.support.statesOrder, state.support.statesCities);
            vehicle["startLocation"] = action.data.currentLocation;
          }
          return vehicle;}
      );
      return fromJS(state).updateIn(['vehicles'], () => formattedResult).toJS();
    }

    case types.LETS_CONNECT_CLICKED: {
      return state;
    }

    case types.SHOW_ERROR_MESSAGE: {
      alert(action.data);
      return state;
    }

    default:
      return state;
  }
}

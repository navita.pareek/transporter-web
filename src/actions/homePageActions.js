import * as types from '../constants/actionTypes';
import * as msg from '../constants/CoreErrors';
import APIService from '../constants/APIService.js';
import history from '../history';

export function onLoginClicked(phoneNumber, password) {
  return function (dispatch) {
    APIService.loginUser({
      "phoneNumber":phoneNumber,
      "password":password
    }, (response) => {
        if(!isError(response)){
          dispatch({
            type: types.ON_LOGIN_CLICKED,
            data: response
          });
          history.push({ pathname: "/dashboard/searchVehicle" });
        }else {
          alert(response);
        }
    });
  };
}

export function logoutUser() {
  return function (dispatch) {
    dispatch({
      type: types.CLEAR_USER_INFO,
      data: ""
    });

    dispatch({
      type: types.CLEAR_DASHBOARD_INFO,
      data: ""
    });
  };
}

export function onRegisterClicked(phoneNumber, password, dateOfBirth, language) {
  return function (dispatch) {
    APIService.registerUser({
      "phoneNumber":phoneNumber,
      "password":password,
      "dateOfBirth":dateOfBirth,
      "language":language
    }, (response) => {
      if(!isError(response)){
        dispatch({
          type: types.ON_REGISTER_CLICKED,
          data: phoneNumber
        });
      }else {
        alert(response);
      }
    });
  };
}

function isError(dataMsg){
  switch(dataMsg){
    case msg.REQUIRED_INPUTS_ARE_NOT_PROVIDED:
    case msg.USER_NOT_REGISTERED:
    case msg.SERVER_UNDER_MAINTENANCE:
    case msg.USER_ALREADY_REGISTERED:
    case msg.INVALID_INFORMATION_PROVIDED:
      return true;
  }

  return false;
}

export function getStateCities(phoneNumber, language) {
  return function (dispatch) {
    APIService.getStateCities(phoneNumber,language
    , (response) => {
      if(!isError(response)){
        dispatch({
          type: types.FETCH_STATES_CITIES,
          data: response
        });
      }else {
        alert(response);
      }
    });
  };
}

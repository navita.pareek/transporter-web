import * as types from '../constants/actionTypes';
import * as msg from '../constants/CoreErrors';
import APIService from '../constants/APIService.js';
import * as util from "../common/util";

export function onSearchVehicleClick(phoneNumber, startDate, startState, startCity, endState, endCity, vehicleCapacityRequired, visible) {
  return function (dispatch) {
    APIService.searchVehicle({
      "phoneNumber":phoneNumber,
      "startDate":startDate,
      "startState":startState,
      "startCity":startCity,
      "endState":endState,
      "endCity":endCity,
      "vehicleCapacityRequired":vehicleCapacityRequired,
      "visible":visible
    }, (response) => {
      if(!isError(response)){
        dispatch({
          type: types.SEARCH_VEHICLE_CLICKED,
          data: response
        });
      }else {
        alert(response);
      }
    });
  };
}

export function onSearchLoadClick(phoneNumber, startDate, startState, startCity, endState, endCity, vehicleCapacityRequired) {
  return function (dispatch) {
    APIService.searchLoad({
      "phoneNumber":phoneNumber,
      "startDate":startDate,
      "startState":startState,
      "startCity":startCity,
      "endState":endState,
      "endCity":endCity,
      "vehicleCapacityRequired":vehicleCapacityRequired,
    }, (response) => {
      if(!isError(response)){
        dispatch({
          type: types.SEARCH_LOAD_CLICKED,
          data: response
        });
      }else {
        alert(response);
      }
    });
  };
}

export function onUpdateTripStatusClick(phoneNumber, trip, newStatus, tripIndex) {
  return function (dispatch) {
    const request = {
      "phoneNumber":phoneNumber,
      "startDate":trip.startDate,
      "startState":util.getStateIndexByLocation(trip.startLocation),
      "startCity":util.getCityIndexByLocation(trip.startLocation),
      "endState":util.getStateIndexByLocation(trip.endLocation),
      "endCity":util.getCityIndexByLocation(trip.endLocation),
      "status":trip.status,
      "vehicleCapacityRequired":trip.vehicleCapacityRequired,
      "newStatus":newStatus,
    };
    const actionData = {
      "tripIndex": tripIndex,
      "newStatus":newStatus,
    }
    APIService.updateTripStatus(request, (response) => {
      if(!isError(response)){
        alert("Trip updated successfully.");
        dispatch({
          type: types.UPDATE_TRIP_STATUS_CLICKED,
          data: actionData
        });
      }else {
        alert(response);
      }
    });
  };
}

export function onUpdateVehicleStatusClick(phoneNumber, vehicle, currentState, currentCity, newStatus, vehicleIndex) {
  return function (dispatch) {
    const request = {
      "phoneNumber":phoneNumber,
      "vehicleNumber":vehicle.vehicleNumber,
      "startState":util.getStateIndexByLocation(vehicle.startLocation),
      "startCity":util.getCityIndexByLocation(vehicle.startLocation),
      "endState":util.getStateIndexByLocation(vehicle.endLocation),
      "endCity":util.getCityIndexByLocation(vehicle.endLocation),
      "status":vehicle.status,
      "capacity":vehicle.capacity,
      "currentState":currentState,
      "currentCity":currentCity,
      "newStatus":newStatus,
    };
    const actionData = {
      "vehicleIndex": vehicleIndex,
      "newStatus":newStatus,
      "currentLocation":currentState + ':' + currentCity,
    }
    APIService.updateVehicleStatus(request, (response) => {
      if(!isError(response)){
        alert("Vehicle updated successfully.");
        dispatch({
          type: types.UPDATE_VEHICLE_STATUS_CLICKED,
          data: actionData
        });
      }else {
        alert(response);
      }
    });
  };
}

export function onAddVehicleClick(phoneNumber, vehicleNumber, startState, startCity, endState, endCity, vehicleCapacity,isLinkedToCompany){
  return function (dispatch) {
    APIService.addVehicle({
      "phoneNumber":phoneNumber,
      "vehicleNumber":vehicleNumber,
      "startState":startState,
      "startCity":startCity,
      "endState":endState,
      "endCity":endCity,
      "capacity":vehicleCapacity,
      "isLinkedToCompany":isLinkedToCompany,
    }, (response) => {
      if(!isError(response)){
        alert("Vehicle added successfully.");
        dispatch({
          type: types.ADD_VEHICLE_CLICKED,
          data: vehicleNumber
        });
      }else {
        alert(response);
      }
    });
  };
}

export function onRemoveVehicleClick(phoneNumber, vehicleNumber){
  return function (dispatch) {
    APIService.removeVehicle({
      "phoneNumber":phoneNumber,
      "vehicleNumber":vehicleNumber
    }, (response) => {
      if(!isError(response)){
        alert("Vehicle removed successfully.");
        dispatch({
          type: types.REMOVE_VEHICLE_CLICKED,
          data: vehicleNumber
        });
      }else {
        alert(response);
      }
    });
  };
}

export function onRemoveManpowerClick(phoneNumber, newManpowerPhoneNumber){
  return function (dispatch) {
    APIService.removeManpower({
      "phoneNumber":phoneNumber,
      "newManpowerPhoneNumber":newManpowerPhoneNumber
    }, (response) => {
      if(!isError(response)){
        alert("Member removed successfully.");
        dispatch({
          type: types.REMOVE_MANPOWER_CLICKED,
          data: response
        });
      }else {
        alert(response);
      }
    });
  };
}

export function onAddCompanyClick(phoneNumber, companyName){
  return function (dispatch) {
    APIService.addCompany({
      "phoneNumber":phoneNumber,
      "companyName":companyName
    }, (response) => {
      if(!isError(response)){
        alert("Company added successfully.");
        dispatch({
          type: types.ADD_COMPANY_CLICKED,
          data: companyName
        });
      }else {
        alert(response);
      }
    });
  };
}

export function onAddManpowerClick(phoneNumber, newManpowerPhoneNumber, isEmpowerForStructureChange){
  return function (dispatch) {
    APIService.addManpower({
      "phoneNumber":phoneNumber,
      "newManpowerPhoneNumber":newManpowerPhoneNumber,
      "isEmpowerForStructureChange":isEmpowerForStructureChange,
    }, (response) => {
      if(!isError(response)){
        alert("New member added successfully.");
        dispatch({
          type: types.ADD_MANPOWER_CLICKED,
          data: response
        });
      }else {
        alert(response);
      }
    });
  };
}

export function onLetsConnectClick(phoneNumber, isHappy, wantCallBack){
  return function (dispatch) {
    APIService.updateFeedback({
      "phoneNumber":phoneNumber,
      "isHappy":isHappy,
      "wantToConnect":wantCallBack,
    }, (response) => {
      if(!isError(response)){
        alert("Connect request submitted successfully.");
        dispatch({
          type: types.LETS_CONNECT_CLICKED,
          data: ""
        });
      }else {
        alert(response);
      }
    });
  };
}

export function getMyVehicles(phoneNumber){
  return function (dispatch) {
    APIService.getMyVehicles(phoneNumber, (response) => {
      if(!isError(response)){
        dispatch({
          type: types.GET_MY_VEHICLES,
          data: response
        });
      }else {
        alert(response);
      }
    });
  };
}

export function getMyTrips(phoneNumber){
  return function (dispatch) {
    APIService.getMyTrips(phoneNumber, (response) => {
      if(!isError(response)){
        dispatch({
          type: types.GET_MY_TRIPS,
          data: response
        });
      }else {
        alert(response);
      }
    });
  };
}

function isError(dataMsg){
  switch(dataMsg){
    case msg.REQUIRED_INPUTS_ARE_NOT_PROVIDED:
    case msg.USER_NOT_REGISTERED:
    case msg.SERVER_UNDER_MAINTENANCE:
    case msg.VEHICLE_ALREADY_REGISTERED:
    case msg.VEHICLE_NOT_REGISTERED:
    case msg.USER_NOT_REGISTER_COMPANY:
    case msg.COMPANY_NOT_REGISTERED:
    case msg.DUPLICATE_REQUEST:
    case msg.COMPANY_REGISTEREDUNABLE_TO_FETCH_DETAILS:
    case msg.COMPANY_REGISTEREDCONTACT_FOR_MANPOWER:
    case msg.USER_NOT_ELIGIBLE_TO_ADD_MANPOWER:
    case msg.USER_IS_ALREADY_WORKING_FOR_SOME_COMPANY:
    case msg.USER_NOT_IN_SAME_COMPANY:

      return true;
  }

  return false;
}
